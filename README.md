# GoHugo Kroki

Go Hugo [Kroki!](https://kroki.io) diagram rendering support.

- [ShortCodes](shortcode.md)
- [Static / JS Lib](static/js-lib/README.md)
- Kroki! Markup Inline Rendering Code Blocks (like built-in Goat)
  - [x] BPMN
  - [x] Ditaa
  - [x] Excalidraw
  - [x] Mermaid
  - [x] PlantUML
