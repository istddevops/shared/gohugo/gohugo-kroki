# BPMN-rendering

```bpmn
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1pbqd41" targetNamespace="http://bpmn.io/schema/bpmn" exporter="bpmn-js (https://demo.bpmn.io)" exporterVersion="8.8.3">
  <bpmn:collaboration id="Collaboration_1tjtjf6">
    <bpmn:participant id="Participant_0sl6mhh" name="Conversie uitvoeren" processRef="Process_1jncm4q" />
  </bpmn:collaboration>
  <bpmn:process id="Process_1jncm4q" isExecutable="false">
    <bpmn:dataStoreReference id="DataStoreReference_1eg95s1" name="GitLab Pages Site" />
    <bpmn:dataStoreReference id="DataStoreReference_1vsdbia" name="Bizz Excel Exports" />
    <bpmn:task id="Activity_039bxps" name="Exports importeren">
      <bpmn:incoming>Flow_0rk0u6d</bpmn:incoming>
      <bpmn:outgoing>Flow_1bko6x1</bpmn:outgoing>
      <bpmn:property id="Property_18sc5d5" name="__targetRef_placeholder" />
      <bpmn:dataInputAssociation id="DataInputAssociation_0748ykf">
        <bpmn:sourceRef>DataStoreReference_1vsdbia</bpmn:sourceRef>
        <bpmn:targetRef>Property_18sc5d5</bpmn:targetRef>
      </bpmn:dataInputAssociation>
      <bpmn:dataOutputAssociation id="DataOutputAssociation_1aukdwq">
        <bpmn:targetRef>DataObjectReference_0fwy5tc</bpmn:targetRef>
      </bpmn:dataOutputAssociation>
    </bpmn:task>
    <bpmn:task id="Activity_020mcei" name="Site genereren">
      <bpmn:incoming>Flow_1bko6x1</bpmn:incoming>
      <bpmn:outgoing>Flow_16scgbb</bpmn:outgoing>
      <bpmn:property id="Property_0m73cyr" name="__targetRef_placeholder" />
      <bpmn:dataInputAssociation id="DataInputAssociation_1373naw">
        <bpmn:sourceRef>DataObjectReference_0fwy5tc</bpmn:sourceRef>
        <bpmn:targetRef>Property_0m73cyr</bpmn:targetRef>
      </bpmn:dataInputAssociation>
      <bpmn:dataOutputAssociation id="DataOutputAssociation_03p136t">
        <bpmn:targetRef>DataStoreReference_1eg95s1</bpmn:targetRef>
      </bpmn:dataOutputAssociation>
    </bpmn:task>
    <bpmn:dataObjectReference id="DataObjectReference_0fwy5tc" name="Markdown Content" dataObjectRef="DataObject_04ymnf7" />
    <bpmn:dataObject id="DataObject_04ymnf7" />
    <bpmn:startEvent id="StartEvent_1nvhuws" name="Conversie is gestart">
      <bpmn:outgoing>Flow_0rk0u6d</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:endEvent id="Event_16e95wo" name="Conversie is uitgevoerd">
      <bpmn:incoming>Flow_16scgbb</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0rk0u6d" sourceRef="StartEvent_1nvhuws" targetRef="Activity_039bxps" />
    <bpmn:sequenceFlow id="Flow_1bko6x1" sourceRef="Activity_039bxps" targetRef="Activity_020mcei" />
    <bpmn:sequenceFlow id="Flow_16scgbb" sourceRef="Activity_020mcei" targetRef="Event_16e95wo" />
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Collaboration_1tjtjf6">
      <bpmndi:BPMNShape id="Participant_0sl6mhh_di" bpmnElement="Participant_0sl6mhh" isHorizontal="true">
        <dc:Bounds x="220" y="75" width="490" height="335" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Flow_0rk0u6d_di" bpmnElement="Flow_0rk0u6d">
        <di:waypoint x="318" y="240" />
        <di:waypoint x="370" y="240" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1bko6x1_di" bpmnElement="Flow_1bko6x1">
        <di:waypoint x="470" y="240" />
        <di:waypoint x="510" y="240" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_16scgbb_di" bpmnElement="Flow_16scgbb">
        <di:waypoint x="610" y="240" />
        <di:waypoint x="652" y="240" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Activity_039bxps_di" bpmnElement="Activity_039bxps">
        <dc:Bounds x="370" y="200" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_020mcei_di" bpmnElement="Activity_020mcei">
        <dc:Bounds x="510" y="200" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="DataObjectReference_0fwy5tc_di" bpmnElement="DataObjectReference_0fwy5tc">
        <dc:Bounds x="482" y="325" width="36" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="457" y="382" width="87" height="27" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1nvhuws">
        <dc:Bounds x="282" y="222" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="270" y="265" width="61" height="27" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_16e95wo_di" bpmnElement="Event_16e95wo">
        <dc:Bounds x="652" y="222" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="641" y="265" width="61" height="27" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="DataStoreReference_1vsdbia_di" bpmnElement="DataStoreReference_1vsdbia">
        <dc:Bounds x="155" y="95" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="155" y="152" width="51" height="27" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="DataStoreReference_1eg95s1_di" bpmnElement="DataStoreReference_1eg95s1">
        <dc:Bounds x="755" y="95" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="736" y="152" width="89" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="DataInputAssociation_0748ykf_di" bpmnElement="DataInputAssociation_0748ykf">
        <di:waypoint x="205" y="120" />
        <di:waypoint x="420" y="120" />
        <di:waypoint x="420" y="200" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="DataOutputAssociation_1aukdwq_di" bpmnElement="DataOutputAssociation_1aukdwq">
        <di:waypoint x="420" y="280" />
        <di:waypoint x="420" y="350" />
        <di:waypoint x="482" y="350" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="DataInputAssociation_1373naw_di" bpmnElement="DataInputAssociation_1373naw">
        <di:waypoint x="518" y="350" />
        <di:waypoint x="560" y="350" />
        <di:waypoint x="560" y="280" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="DataOutputAssociation_03p136t_di" bpmnElement="DataOutputAssociation_03p136t">
        <di:waypoint x="560" y="200" />
        <di:waypoint x="560" y="120" />
        <di:waypoint x="755" y="120" />
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>
```

