# PlantUML-rendering

```plantuml
@startuml
hide methods

package "ontoUML-uitwisselingsformaat" {

    abstract Object {
        id
        name
        description
    }

    class Package {
        propertyAssignments
    }

    class Project

    
    
    package "Model" {
        
    }

    package "Diagrammen" {
    
    }
    
    Object <|-- Project
    Object <|-- Package

    Project o-- "Model"
    Project o-- "Diagrammen"
}


@enduml
```
