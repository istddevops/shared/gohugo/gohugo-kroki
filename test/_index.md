# Kroki! Test

Test-publicatie *kroki!* Layout voor weergave via de **HUGO** Sitegenerator.

```plantuml
@startuml
"<b>HUGO</b> Sitegenerator" -> "<i>kroki!</i> Layout" :  tekst-codering
"<i>kroki!</i> Layout" -> "<i>kroki!</i> Api" : opvragen weergave
"<i>kroki!</i> Layout" <- "<i>kroki!</i> Api" : ontvangen weergave
"<b>HUGO</b> Sitegenerator" <- "<i>kroki!</i> Layout": diagram weergave
@enduml
```
