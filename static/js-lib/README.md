# Kroki! Javascript Assets

HUGO Javascript Assets voor de aanvullende functionaliteit die wordt gebruikt voor [**Kroki!** Diagramweergaven](kroki.js).

Gebaseerd op [**Kroki.io** setup encode diagram](https://docs.kroki.io/kroki/setup/encode-diagram/#javascript).
